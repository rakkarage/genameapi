FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app
EXPOSE 80

COPY ./GenameApi/GenameApi.csproj ./GenameApi/
RUN dotnet restore ./GenameApi/GenameApi.csproj
COPY . .
RUN dotnet publish ./GenameApi/GenameApi.csproj -c Release -o /app

FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "GenameApi.dll"]
